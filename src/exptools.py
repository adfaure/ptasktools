#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import json
import errno
import sys
import subprocess
import math
from subprocess import Popen, PIPE
import re
import copy
from shutil import copyfile
import os
import yaml
import pandas as pd

__requires__ = ["CherryPy < 3"]  # Must be set before pkg_resources import
from pkg_resources import resource_filename

types = {
    "WORD": r"[A-Za-z0-9\-\.]*",
    "NUMBER": r"\d+",
    "FLOAT": r"[0-9\.]+",
    # todo: extend me
}


def compile(pat):
    return re.sub(
        r"%{(\w+):(\w+)}",
        lambda m: "(?P<" + m.group(2) + ">" + types[m.group(1)] + ")",
        pat,
    )


# Generates a simple homogeneous platform
def generate_dahu_platform_batsim(
    radical=31,
    speed="1",
    bandwidth="12.5GBps",
    latency="0.05us",
    lb_bw="18.348422228142745GBps",
    lb_lat="0.0us",
    write=None,
):
    template = """<?xml version="1.0"?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
    <zone id="AS0" routing="Full">
      <cluster id="cluster_dahu" prefix="dahu-" suffix=".grid5000.fr" radical="0-{radical}"
               speed="{speed}"
               core="32"
               bw="{bandwidth}"
               lat="{latency}"
               loopback_bw="{lb_bw}"
               loopback_lat="{lb_lat}"/>

      <cluster id="my_cluster_1" prefix="master_host" suffix="" radical="0-0"
          speed="1" bw="12.5MBps" lat="50us" bb_bw="2.25GBps"
          bb_lat="500us">
      </cluster
      <link id="backbone" bandwidth="1000.25GBps" latency="0.01us" />
      <zoneRoute src="cluster_dahu" dst="my_cluster_1" gw_src="dahu-cluster_dahu_router.grid5000.fr"
          gw_dst="master_hostmy_cluster_1_router">
          <link_ctn id="backbone" />
      </zoneRoute>
    </zone>
</platform>
"""
    platform = template.format(
        size=size - 1,
        speed=speed,
        bandwidth=bandwidth,
        lb_bw=lb_bw,
        lb_lat=lb_lat,
        latency=latency,
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(platform, file=tfile)

    return platform


# Generates a simple homogeneous platform
def generate_homogeneous_platform(
    size,
    speed="3Gf",
    bandwidth="125MBps",
    latency="50us",
    bb_bw="2.25GBps",
    bb_lat="500us",
    write=None,
):
    template = """<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">

<AS id="AS0" routing="Full">
    <cluster id="my_cluster_1" prefix="a" suffix="" radical="0-{size}"
        speed="{speed}" bw="{bandwidth}" lat="{latency}" bb_bw="{bb_bw}"
        bb_lat="{bb_lat}" />

    <cluster id="my_cluster_2" prefix="master_host" suffix="" radical="0-0"
        speed="1Gf" bw="125MBps" lat="50us" bb_bw="2.25GBps"
        bb_lat="500us" />

     <link id="backbone" bandwidth="1.25GBps" latency="500us" />

    <ASroute src="my_cluster_1" dst="my_cluster_2" gw_src="amy_cluster_1_router"
        gw_dst="master_hostmy_cluster_2_router">
        <link_ctn id="backbone" />
    </ASroute>
</AS>
</platform>
"""

    platform = template.format(
        size=size - 1,
        speed=speed,
        bandwidth=bandwidth,
        bb_bw=bb_bw,
        bb_lat=bb_lat,
        latency=latency,
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(platform, file=tfile)

    return platform


def generates_ptask_profile(
    name,
    cpu=[5e6, 0, 0, 0],
    com=[5e6, 0, 0, 0, 5e6, 5e6, 0, 0, 5e6, 5e6, 0, 0, 5e6, 5e6, 5e6, 0],
):
    profile = {name: {"type": "parallel", "cpu": cpu, "com": com}}
    return profile


def generates_ptask_profile_sequence(name, profiles=[], repeat=1):
    profile_names = []
    for profile in profiles:
        profile_names.append(next(iter(profile)))

    profile_seq = {"type": "composed", "repeat": repeat, "seq": profile_names}

    return ({name: profile_seq}, profiles)


def generates_ptask_homogeneous_profile(name, cpu=10e5, com=1e6):
    profile = {name: {"type": "parallel_homogeneous", "cpu": cpu, "com": com}}
    return profile


def gen_job(id_job="default", subtime=0, walltime=-1, res=1, profile="no-pro", data={}):
    job = {
        "id": str(id_job),
        "subtime": subtime,
        "walltime": walltime,
        "res": res,
        "profile": profile,
    }
    return {**job, **data}


def gen_workload(
    jobs, profiles_list, nb_res, write=None, asfmt="object", directory=None
):
    """
    `directory`: This parameter is mandatory to generate a workload with TIT traces.
                 This is because, the path to the files needs to be relative to the workload file.
    """
    profiles_dict = {}
    # We replace the profile of the job by the
    # profile name, and we gather the profile.
    for profile in profiles_list:
        profiles_dict = {**profiles_dict, **profile}

    workload = {
        "nb_res": nb_res,  # Integer
        "jobs": jobs,  # Must be an array
        "profiles": profiles_dict,  # Must be a dict
    }

    if asfmt is "json":
        return json.dumps(workload, sort_keys=True, indent=2, separators=(",", ": "))

    if write is not None:
        with open(write, "w") as tfile:
            print(
                json.dumps(workload, sort_keys=True, indent=2, separators=(",", ": ")),
                file=tfile,
            )
    return workload


def _write_smpi_profile(profile, profile_name, workload_path):
    workload_basedir = os.path.dirname(workload_path)
    trace_file_content = []
    trace_file_path = os.path.join(
        workload_basedir, os.path.basename(profile_name + "_traces")
    )
    profile_files_dir = os.path.join(workload_basedir, profile_name)

    if not os.path.exists(profile_files_dir):
        os.makedirs(profile_files_dir)

    with open(profile["trace"]) as input_file:
        for _, line in enumerate(input_file):
            rank_tracefile_path = line.replace("\n", "")
            rank_tracefile_name = os.path.basename(rank_tracefile_path)

            # For each files of the trace, we need to find its path relatively
            # to the workload file, as it is implemented into batsim.
            copyfile(
                rank_tracefile_path,
                os.path.join(profile_files_dir, rank_tracefile_name),
            )
            trace_file_content.append(os.path.join(profile_name, rank_tracefile_name))

    with open(trace_file_path, "w") as file:
        print("\n".join(trace_file_content), file=file)

    # The tracefile provided to batsim into the workload shall be relative to the workload file.
    # As the file is writtent in to same folder, giving the name is sufficient
    profile["trace"] = os.path.basename(trace_file_path)


def write_json_workload(workload, workload_path):
    for profile_name, profile in workload["profiles"].items():
        if profile["type"] == "smpi":
            _write_smpi_profile(profile, profile_name, workload_path)

    with open(workload_path, "w") as tfile:
        print(
            json.dumps(workload, sort_keys=True, indent=2, separators=(",", ": ")),
            file=tfile,
        )


def gen_robin(batsim_command, sched_command, outdir, write=None):

    rob_template = """batcmd: {batsim_command}
schedcmd: {sched_command}
output-dir: {outdir}
simulation-timeout: 3000
ready-timeout: 100
success-timeout: 1000
failure-timeout: 10
"""
    output = rob_template.format(
        batsim_command=batsim_command, sched_command=sched_command, outdir=outdir
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(output, file=tfile)

    return output


def prepare_and_run(
    base_directory=None,
    platform=None,
    workload=None,
    sched_command="batsched",
    instance_name="default",
    batsim_extra_params="",
    mpi_tracing=False,
    master="master_host0",
):

    instance_directory = os.path.join(base_directory, instance_name)
    input_dir = os.path.join(instance_directory, "inputs")
    platform_path = os.path.join(input_dir, "platform.xml")
    workload_path = os.path.join(
        input_dir, "workload_{instance_name}.json".format(instance_name=instance_name)
    )

    # the actual code
    try:
        os.makedirs(input_dir)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(input_dir):
            pass

    with open(platform_path, "w") as tfile:
        print(platform, file=tfile)

    write_json_workload(workload, workload_path)

    robin_file = os.path.join(instance_directory, "robin.yaml")
    simulation_results = os.path.join(base_directory, instance_name, "outputs")

    if mpi_tracing:
        batsim_extra_params += " ".join(
            [
                "--sg-cfg tracing:yes",
                "--sg-cfg tracing/uncategorized:yes",
                "--sg-cfg tracing/filename:{result_dir}/simgrid.paje",
            ]
        ).format(result_dir=simulation_results)

    batsim_command = "batsim {batsim_extra_params} -p {platform} -w {workload} -m {master} -e {outputfolder}/out".format(
        batsim_extra_params=batsim_extra_params,
        platform=platform_path,
        workload=workload_path,
        master=master,
        outputfolder=simulation_results,
    )

    # Robin generation
    gen_robin(batsim_command, sched_command, simulation_results, write=robin_file)

    robin_p = subprocess.Popen(["robin", robin_file], stdout=PIPE, stderr=PIPE)
    output, err = robin_p.communicate()

    return (output, err, instance_directory)


# Paje and SMPI related
def a_sends_to_b(com=[], a=0, b=0, size=0):
    # print(str(a) + " sends to " + str(b))
    nb_proc = int(math.sqrt(len(com)))
    col_b = int(b % nb_proc)

    com[(a * nb_proc) + col_b] = com[(a * nb_proc) + col_b] + size
    return com


def log_square_matrix(com=[]):
    nb_proc = int(math.sqrt(len(com)))
    for i in range(nb_proc):
        for j in range(nb_proc):
            sys.stdout.write(str(com[(nb_proc * i) + j]) + "\t")
        sys.stdout.write("\n")


def creates_communication_matrix_from_smpipaje(
    communication_matrix=None, filename=None
):
    # Get the link transfert
    links = [a.rstrip() for a in open(filename).readlines() if a.startswith("15")]
    for link in links:
        # We trace only point to point operations
        if not "PTP" in link:
            continue
        split = link.split()
        key = split[6].split("_")
        s = int(split[7])
        from_ = int(key[0]) - 1
        to_ = int(key[1]) - 1
        communication_matrix = a_sends_to_b(
            com=communication_matrix, a=from_, b=to_, size=s
        )
    return communication_matrix


def run_smpi(
    nb_rank=4,
    smpi_binary_with_param="",
    hostfile="",
    platform="",
    name="default",
    smpi_extra_params="",
):
    # for simgrid
    command_array = [
        "smpirun",
        "-map -np {nb_rank}",
        "-hostfile {hostfile}",
        "-platform {platform}",
        "{smpi_extra_params}",
        "{smpi_binary_with_param}",
    ]

    command = " ".join(command_array).format(
        nb_rank=nb_rank,
        hostfile=hostfile,
        platform=platform,
        smpi_binary_with_param=smpi_binary_with_param,
        smpi_extra_params=smpi_extra_params,
    )

    # Run the SMPI command
    # Note that the parameter shell=True allows us to send a full command instead of an array of parameters.
    p = subprocess.Popen(command, shell=True, stdout=PIPE, stderr=PIPE)

    output, err = p.communicate()
    return (output, err, command)


def run_eztrace_mpi(
    message_size=1024, nbrank=4, tracedire=".", mpi_binary_with_param=""
):
    hostfile = "hostfiles/localhost"

    command = [
        "EZTRACE_TRACE='mpi'",
        f"mpirun -hostfile {hostfile}",
        f"-n {nbrank}",
        f"eztrace -o {tracedire}",
        f"{mpi_binary_with_param}",
    ]

    dump_msg_command = [
        "EZTRACE_MPI_DUMP_MESSAGES=1",
        "eztrace_stats",
        f"-o {tracedire}/stats",
        f"{tracedire}/adfaure_eztrace_log_rank_*",
    ]

    # Launching mpi
    mpi_out = subprocess.check_output(
        " ".join(command), shell=True, stderr=subprocess.STDOUT
    )
    print(mpi_out)
    # Building the paje trace
    # !nix-shell gemmpi/shell.nix --command "eztrace_convert -o eztrace/4rank-size16.paje eztrace/adfaure_eztrace_*"
    dump_out = subprocess.check_output(
        " ".join(dump_msg_command), shell=True, stderr=subprocess.STDOUT
    )
    print(dump_out)
    output = subprocess.check_output(
        ["cat", tracedire + "/stats/communication_matrix_collective.message_size.dat"]
    )

    # output is a string, we convert it to an array of values
    eztrace_communication_matrix = []
    for test in output.decode("utf-8").split("\n"):
        values = test.split()
        int_values = [int(x) for x in values]
        eztrace_communication_matrix += int_values

    return eztrace_communication_matrix


def merge_communication_matrix(
    communication_matrix=[],
    hosts=[],
    computation_matrix=[],
    mapping=None,
    nb_rank=0,
    nb_hosts=0,
):
    """
    merge the matrix according to a mapping and a number of host.
    `computation_matrix` is the actual matrix
    `mapping` is a mapping from `create_mapping_from_smpirun_output`
    `nb_host` is the new matrix size (can be optain from the return of create_mapping_from_smpirun_output)
    """
    new_comp = [0] * nb_hosts
    new_comms = [0] * nb_hosts * nb_hosts
    loop = 0

    next_id = 0
    hostid_mapped_by_hostname = {}
    for host in hosts:
        hostid_mapped_by_hostname[host] = next_id
        next_id += 1

    for (rank, host) in mapping.items():
        rank_ = int(rank)
        host_ = hostid_mapped_by_hostname[host]
        rank_first_idx = nb_hosts * loop
        # print("rank_", rank_, "on host", host, "->", communication_matrix[rank_first_idx:rank_first_idx+nb_rank])
        for (dest_rank, size) in zip(
            list(range(0, nb_rank)),
            communication_matrix[rank_first_idx : rank_first_idx + nb_rank],
        ):
            dest_host = mapping[str(dest_rank)]
            dest_host_ = hostid_mapped_by_hostname[dest_host]
            # print("src:",host_,"dest:", dest_host, "size:", size)
            new_comms = a_sends_to_b(
                a=host_, b=int(dest_host_), com=new_comms, size=size
            )

        new_comp[host_] = +computation_matrix[rank_]

        loop += 1
    return (new_comms, new_comp, hostid_mapped_by_hostname)


def generate_hostfile_dahu(nb_core=32, nb_host=32, write=None, round_robin=True):
    host_template = "dahu-{}.grid5000.fr\n"
    hosts = []

    # We gather as many cores as we need, looping through each hosts
    for core_id in range(0, nb_core):
        hosts.append(core_id % nb_host)

    # If round robin is not requested, we sort the array
    if not round_robin:
        hosts.sort()

    # Apply the template on each rank, and join them to contruct a string
    hostsfile_content = "".join(
        map(lambda host_id: host_template.format(host_id), hosts)
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(hostsfile_content, file=tfile)
    else:
        print(hostsfile_content)


# Generates a simple homogeneous platform
def generate_dahu_platform_smpi(
    radical=32,
    speed="1",
    bandwidth="12.5GBps",
    latency="0.05us",
    lb_bw="18.348422228142745GBps",
    lb_lat="0.0us",
    write=None,
):
    template = """<?xml version="1.0"?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
    <config id="General">
        <prop id="smpi/os" value="0:2.965491847282898e-07:9.63619592297199e-11;8133:4.117386759387727e-06:9.998650637323814e-11;15831:3.901910334928982e-06:1.1003492469181915e-10;33956:8.556208528368661e-06:1.2636592924112397e-10;64000:0.0:0.0"/>
        <prop id="smpi/or" value="0:1.3754300044375236e-06:8.456019002037162e-11;8133:1.0616877366305338e-06:1.0399801951417905e-10;15831:1.1989168715510673e-06:9.880880432093221e-11;33956:1.3028804423006338e-06:1.0742263180618875e-10;64000:0.0:0.0"/>
        <prop id="smpi/ois" value="0à:6.931229521083401e-07:7.051204536228214e-11;8133:3.601990790153244e-07:1.2449128895712037e-10;15831:2.97701688103096e-06:4.1302612649640425e-11;33956:3.133466154066955e-06:3.293458765281899e-11;64000:6.939993663604069e-07:0.0"/>
        <prop id="smpi/bw-factor" value="0:0.4955575510466301;8133:5.649491428460505;15831:5.716405752533658;33956:12.659099698925065;64000:0.9867951082730274"/>
        <prop id="smpi/lat-factor" value="0:1.2162964660682605;8133:19.230103984475342;15831:18.285606440676755;33956:51.6103738900493;64000:129.24904864615127"/>
        <prop id="smpi/async-small-thresh" value="64000"/>
        <prop id="smpi/send-is-detached-thresh" value="64000"/>
        <prop id="smpi/iprobe" value="2.115437983643232e-07"/>
        <prop id="smpi/test" value="8.090518732515405e-07"/>
        <prop id="smpi/host-speed" value="1"/>
    </config>
    <cluster id="cluster_dahu" prefix="dahu-" suffix=".grid5000.fr" radical="0-{radical}" 
                speed="{speed}" 
                core="32"
                bw="{bandwidth}" 
                lat="{latency}" 
                loopback_bw="{lb_bw}" 
                loopback_lat="{lb_lat}"/>
</platform>
"""
    platform = template.format(
        radical=radical - 1,
        speed=speed,
        bandwidth=bandwidth,
        lb_bw=lb_bw,
        lb_lat=lb_lat,
        latency=latency,
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(platform, file=tfile)

    return platform


# Generates a simple homogeneous platform
def generate_dahu_platform_batsim(
    radical=32,
    speed="1",
    bandwidth="12.5GBps",
    latency="0.05us",
    lb_bw="18.348422228142745GBps",
    lb_lat="0.0us",
    core=32,
    write=None,
):
    template = """<?xml version="1.0"?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">  
    <zone id="AS0" routing="Full">
      <cluster id="cluster_dahu" prefix="dahu-" suffix=".grid5000.fr" radical="0-{radical}" 
               speed="{speed}" 
               core="{core}"
               bw="{bandwidth}" 
               lat="{latency}" 
               loopback_bw="{lb_bw}" 
               loopback_lat="{lb_lat}"/>

      <cluster id="my_cluster_1" prefix="master_host" suffix="" radical="0-0"
          speed="1" bw="12.5MBps" lat="50us" bb_bw="2.25GBps"
          bb_lat="500us">
      </cluster>
      <link id="backbone" bandwidth="1000.25GBps" latency="0.01us" />
      <zoneRoute src="cluster_dahu" dst="my_cluster_1" gw_src="dahu-cluster_dahu_router.grid5000.fr"
          gw_dst="master_hostmy_cluster_1_router">
          <link_ctn id="backbone" />
      </zoneRoute>
    </zone>
</platform>
"""
    platform = template.format(
        radical=radical - 1,
        speed=speed,
        bandwidth=bandwidth,
        lb_bw=lb_bw,
        lb_lat=lb_lat,
        core=core,
        latency=latency,
    )

    if write is not None:
        with open(write, "w") as tfile:
            print(platform, file=tfile)

    return platform


def create_mapping_from_smpirun_output(output):
    # rr = compile("\[rank %{NUMBER:rank}\] -> %{WORD:hostname}-%{NUMBER:hostid}.%{WORD:domain}")
    rr = compile(
        "\[rank %{NUMBER:rank}\] -> %{WORD:hostname}-%{NUMBER:hostid}.%{WORD:domain}"
    )

    mapping = [a.rstrip() for a in output.splitlines() if a.startswith("[rank ")]
    # print(mapping)
    mapping_final = dict()
    hosts = []
    for maps in mapping:
        dictt = re.search(rr, maps).groupdict()
        # print(dictt)
        hostid = dictt["hostid"]
        rank = dictt["rank"]
        mapping_final[rank] = hostid
        hosts.append(hostid)
    return (mapping_final, list(set(hosts)))


def create_mapping_from_smpirun_output2(output):

    mapping = [a.rstrip() for a in output.splitlines() if "[rank " in a]
    mapping_final = dict()
    hosts = []
    for maps in mapping:
        match = re.search(r"\[rank (\d+)\] -> ([A-Za-z0-9\-\.]+)", maps)

        rank = match.group(1)
        hostname = match.group(2)

        mapping_final[rank] = hostname
        hosts.append(hostname)
    return (mapping_final, list(set(hosts)))


def creates_communication_matrix_from_smpipaje_with_intervals(
    nb_rank=0,
    filename=None,
    intervals=[(0, -float("Inf"), float("Inf"))],
    include_bound=True,
):
    """
    Create a communication matrix from a paje trace.

    :param int nb_rank: \
            The number of processes involved into the trace file.
    :param str filename: \
            The path to the paje trace file generated from SMPI.
    :param array intervals: \
            A list of tuples, of intervals example: [(0, 0.001, 0.10), (1, 0.10, 0.13)]
            Note that the inferior bound of each interval is inclusive, while the higher is exclusive.
    """
    intervals_matrixes = dict()

    if include_bound:
        intervals[0] = (intervals[0][0], -float("Inf"), intervals[0][2])
        intervals[-1] = (intervals[-1][0], intervals[-1][1], float("Inf"))

    for interval in intervals:
        intervals_matrixes[interval[0]] = [0] * nb_rank * nb_rank

    links = [a.rstrip() for a in open(filename).readlines() if a.startswith("15")]
    for link in links:
        # print(link)
        split = link.split()
        timestamp = split[1]
        key = split[6].split("_")
        s = int(split[7])
        from_ = int(key[0]) - 1
        to_ = int(key[1]) - 1
        found_attached_loop = False
        for (id_, start, end) in intervals:
            if float(timestamp) >= start and float(timestamp) < end:
                found_attached_loop = True
                intervals_matrixes[id_] = a_sends_to_b(
                    com=intervals_matrixes[id_], a=from_, b=to_, size=s
                )

        if not found_attached_loop:
            pass
            # print("orphan link", link)
    return intervals_matrixes


def play_gemmpi_smpi(
    expdir="/tmp",
    nb_host_dahu=32,
    flops_dahu=1,
    nbrank=1024,
    matrice_dims=1024,
    nb_subdivisions=1,
    name_execution=None,
    platform_file=None,
    hostfile=None,
    no_exec=False,
    trace_tit=False,
    extra_smpi_params="",
    smpi_binary=None,
    nb_max_loop=None,
    second_per_flops=6.864500e-11,
):
    """
    `matrice_dims`: dimensions of the square matrix.
    `no_exec` bool: usefull to check the parameters before launching the simulation.

    The beginning of the function computes some metrics.
    The first thing to compute, is depending on the matrix size and the number of processes, what
    will be the number of work on each ranks.

    second_per_flops, is a value computed by Tom, it depends on dahu.
    flops_per_core is the speed of a core.
    flops_per_host, however depends on the number of rank and the size of the platform.
        It represents the speed of a host times the number of rank mapped on each host.
        It comes from the fact that pdgemm application does not necessarily use all the cores of the machine,
        this value is intend to be used by batsim.
    """
    # yml = yaml.YAML()
    matrice_size = matrice_dims * matrice_dims

    block_size = matrice_size / nbrank
    block_len = math.sqrt(block_size)

    division_len = math.floor(block_len / nb_subdivisions)
    division_size = block_len * division_len

    rank_per_host = nbrank / nb_host_dahu
    flops_per_core = 1 / second_per_flops
    flops_per_host = rank_per_host * flops_per_core

    mnk = float(division_len * block_len * block_len)

    if no_exec:
        print("matrice_dims", matrice_dims, "matrice size: ", matrice_size)
        print("block_len", block_len, "block size: ", block_size)
        print("division_dimensions", division_len, "x", block_len)
        print(
            "number of rank per host:",
            rank_per_host,
        )
        print("Gflops = ", flops_per_host, ", mnk", mnk)
        print("one matmut should take on one rank=", mnk / flops_per_core, "s")
        print(
            "total memory size of SMPI simulation=",
            (matrice_size * 5 * 8) / math.pow(10, 9),
            "GB",
        )

        return (None, None)

    prefix = "tit_" if trace_tit else "paje_"
    if name_execution is None:
        name_execution = prefix + "nbhost-{}_nbrank-{}_dims-{}_subdivisions-{}".format(
            nb_host_dahu, nbrank, matrice_dims, nb_subdivisions
        )

    result_dir = os.path.join(expdir, name_execution)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    if smpi_binary is None:
        smpi_binary = "gemsmpi -s {nb_subdivisions} {matrice_dims}".format(
            nb_subdivisions=nb_subdivisions, matrice_dims=matrice_dims
        )

    if nb_max_loop is not None:
        smpi_binary += " -l {}".format(nb_max_loop)

    files = {
        "metadata": os.path.join(result_dir, "metadata.yaml"),
        "hostfile": os.path.join(result_dir, "dahu-hostfile"),
        "platform": os.path.join(result_dir, "dahu-generated.xml"),
        "smpi_out": os.path.join(result_dir, "smpi_out"),
        "smpi_err": os.path.join(result_dir, "smpi_err"),
        "marks_csv": os.path.join(result_dir, "rank_actions.csv"),
        "loops_csv": os.path.join(result_dir, "aggregated_events.csv"),
    }

    if hostfile is None:
        generate_hostfile_dahu(
            nb_core=nbrank,
            nb_host=nb_host_dahu,
            round_robin=False,
            write=files["hostfile"],
        )
    else:
        files["hostfile"] = hostfile
        copyfile(hostfile, result_dir + "/hostfile")

    if platform_file is None:
        generate_dahu_platform_smpi(
            write=files["platform"],
            speed=str(flops_per_core / 1e9) + "Gf",
            radical=nb_host_dahu,
        )
    else:
        files["platform"] = platform_file
        copyfile(platform_file, result_dir + "/platform.xml")

    smpi_params_tit = ""
    smpi_params_paje = ""

    # Unfortunatelly paje and tit cannot be ran together
    if trace_tit:
        trace_name_tit = os.path.join(result_dir, "smpi_trace.tit")
        files["tit_trace"] = trace_name_tit
        smpi_params_tit = "-trace-ti -trace-file {trace_name_tit} ".format(
            trace_name_tit=trace_name_tit
        )
    else:
        trace_name_paje = os.path.join(result_dir, "smpi_trace.paje")
        files["paje_trace"] = trace_name_paje
        smpi_params_paje = " ".join(
            [
                "-trace -trace-file {trace_name_paje} ",
                "--cfg=tracing/smpi/display-sizes:1",
                # "--cfg=tracing/uncategorized:yes"
                "--cfg=tracing/smpi/internals:1",
            ]
        ).format(trace_name_paje=trace_name_paje)

    # Register extra params to print the simulation time
    smpi_extra_params = (
        " --cfg=smpi/display-timing:1 "
        + smpi_params_paje
        + " "
        + smpi_params_tit
        + " "
        + extra_smpi_params
    )

    # Run smpi simulation
    (smpi_output, smpi_err, smpi_cmd) = run_smpi(
        nb_rank=nbrank,
        hostfile=files["hostfile"],
        platform=files["platform"],
        smpi_binary_with_param=smpi_binary,
        smpi_extra_params=smpi_extra_params,
    )
    print(smpi_cmd)
    print(smpi_err)
    # Only for gemsmpi application
    # Log outputs
    with open(files["smpi_out"], "w") as tfile:
        print(smpi_output.decode(), file=tfile)
    with open(files["smpi_err"], "w") as tfile:
        print(smpi_err.decode(), file=tfile)

    # Gather logged events and
    csv = "rank,action,action_event,loop,time\n"
    for line in smpi_output.decode().splitlines():
        if "trace," in line:
            csv_line = line.replace("trace,", "")
            csv += csv_line + "\n"
    with open(files["marks_csv"], "w") as tfile:
        print(csv, file=tfile)

    r_script = resource_filename(__name__, "scripts/create_loop_timestamps.R")
    print("R script:", r_script)
    p = subprocess.Popen(
        [r_script, files["marks_csv"], files["loops_csv"]], stdout=PIPE, stderr=PIPE
    )

    _, err = p.communicate()

    print(smpi_err.decode())
    find_total_time = compile(
        "\[%{FLOAT:total_time_smpi}\] \[smpi_kernel/INFO\] Simulated time: %{FLOAT:total_time_repeat} seconds."
    )
    find_reality_time = compile(
        "The simulation took %{FLOAT:reality_time} seconds \(after parsing and platform setup\)"
    )
    search = re.search(find_total_time, smpi_err.decode())
    if search is not None:
        dictt = search.groupdict()

        total_time_smpi = float(dictt["total_time_smpi"])
        dictt = re.search(find_reality_time, smpi_err.decode()).groupdict()
        reality_time = float(dictt["reality_time"])

    total_time_smpi = None
    dictt = None
    reality_time = None

    metadata = dict()
    metadata = {
        "inputs": {
            "name": name_execution,
            "nb_rank": nbrank,
            "nb_rank_per_host": rank_per_host,
            "nb_host_platform": nb_host_dahu,
            "second_per_flops": second_per_flops,
            "flops_per_core": flops_per_core,
            "flops_per_host": flops_per_host,
            "matrice_dims": matrice_dims,
            "nb_subdivison": nb_subdivisions,
            "block_size": block_size,
            "block_len": block_len,
            "division_len": division_len,
            "division_size": division_size,
            "mnk": mnk,
            "gemmpi_cmd": smpi_binary,
            "smpi_cmd": smpi_cmd,
        },
        "outputs": {
            "reality_time": reality_time,
            "simulation_time": total_time_smpi,
            "result_dir": result_dir,
            "files": files,
        },
    }
    with open(files["metadata"], "w") as file:
        yaml.dump(metadata, file)
    return metadata


def profile_mapping_from_smpi_metadata(smpi_metadata):
    smpi_output_file = smpi_metadata["outputs"]["files"]["smpi_err"]
    with open(smpi_output_file) as smpi_ouput:
        (mapping_ranktohost, hosts) = create_mapping_from_smpirun_output2(
            smpi_ouput.read()
        )
    return (mapping_ranktohost, hosts)


def create_profile_comm_and_task_aggregated(metadata, name=None):
    smpi_err = metadata["outputs"]["files"]["smpi_err"]
    smpi_paje = metadata["outputs"]["files"]["paje_trace"]

    with open(smpi_err, "r") as stream:
        content = stream.read()
        mapping = create_mapping_from_smpirun_output2(content)

    print(mapping)

    nb_rank = len(mapping[0])

    rank_comm_matrix = creates_communication_matrix_from_smpipaje(
        communication_matrix=[0 for x in range(0, nb_rank * nb_rank)],
        filename=smpi_paje,
    )

    mnk = metadata["inputs"]["mnk"]
    # Get the total number of loops
    df = pd.read_csv(metadata["outputs"]["files"]["loops_csv"])
    nb_loop = int(df["loop"].iloc[-1])

    computation_matrix_ranks = [float(mnk * nb_loop) for x in range(nb_rank)]

    (comm_matrix, comp_matrix, hosts) = merge_communication_matrix(
        communication_matrix=rank_comm_matrix,
        computation_matrix=computation_matrix_ranks,
        mapping=mapping[0],
        hosts=mapping[1],
        nb_rank=len(mapping[0]),
        nb_hosts=len(mapping[1]),
    )

    # Create hostfile
    hostfile_aggregated = ""
    hostname_order = [""] * len(mapping[1])
    print(hosts)
    for (key, value) in hosts.items():
        hostname_order[int(value)] = key

    for host in hostname_order:
        hostfile_aggregated += f"{host}\n"

    profile = generates_ptask_profile(name, comp_matrix, comm_matrix)
    print(hostfile_aggregated)

    return (profile, hostfile_aggregated)


def create_profile_with_mapping_from_smpi(metadata, name=None):
    # Init the matrix
    nb_rank = metadata["inputs"]["nb_rank"]
    nb_divisions = metadata["inputs"]["nb_subdivison"]
    mnk = metadata["inputs"]["mnk"]
    tracefile = metadata["outputs"]["files"]["paje_trace"]

    if name is None:
        name = metadata["inputs"]["name"] + "_ranks_with_mapping"

    (mapping_ranktohost, hosts) = profile_mapping_from_smpi_metadata(metadata)
    rank_comm_matrix = creates_communication_matrix_from_smpipaje(
        communication_matrix=[0 for x in range(0, nb_rank * nb_rank)],
        filename=tracefile,
    )

    # Get the total number of loops
    df = pd.read_csv(metadata["outputs"]["files"]["loops_csv"])
    nb_loop = int(df["loop"].iloc[-1])

    computation_matrix_ranks = [float(mnk * nb_loop) for x in range(nb_rank)]

    # Profile generation
    profile_ranks = generates_ptask_profile(
        name, cpu=computation_matrix_ranks, com=rank_comm_matrix
    )
    return (profile_ranks, mapping_ranktohost)


def create_profile_sequence_from_smpi(
    metadata, name=None, merge_com_and_cpu=True, nb_loops_to_aggregate=1
):
    # Init the matrix
    nb_rank = metadata["inputs"]["nb_rank"]
    nb_divisions = metadata["inputs"]["nb_subdivison"]
    mnk = metadata["inputs"]["mnk"]
    tracefile = metadata["outputs"]["files"]["paje_trace"]
    loops_csv = metadata["outputs"]["files"]["loops_csv"]

    profile_name = name
    if name is None:
        profile_name = metadata["inputs"]["name"] + "_sequence"

    intervals = []
    # create intervals from the result of the Rscript -> create_loop_timestamps.Rscript
    gather_begin = True
    wait_for = 0
    df = pd.read_csv(loops_csv)
    max_loop = df["loop"].count()

    for (id_, row) in df.iterrows():
        row[row["loop"]] = []
        if row["loop"] == wait_for:
            if gather_begin:
                start = row["min_bcast_begin"]
                wait_for += nb_loops_to_aggregate - 1
                wait_for = min(wait_for, max_loop)
            else:
                end = row["min_comp_end"]
                # Intervals are tuple of int
                intervals.append((row["loop"], float(start), float(end)))
                wait_for += 1

            gather_begin = not gather_begin
    print(intervals)
    matrixes = creates_communication_matrix_from_smpipaje_with_intervals(
        nb_rank=nb_rank, filename=tracefile, intervals=intervals, include_bound=True
    )
    splitted_comp_matrix = [float(mnk) for x in range(nb_rank)]
    # used in the separated com and cpu profile
    empty_cpu = [0] * nb_rank
    empty_comm = [0] * nb_rank * nb_rank

    seq_profiles = []
    sequence_names = []
    for (key, item) in matrixes.items():
        seq_name = profile_name + "_sequence_{}".format(key)
        sequence_names.append(seq_name)
        if merge_com_and_cpu:
            seq_profile = generates_ptask_profile(
                name=seq_name, com=item, cpu=splitted_comp_matrix
            )
            seq_profiles.append(seq_profile)
        else:
            seq_profile = generates_ptask_profile(
                name=seq_name + "_com", com=item, cpu=empty_cpu
            )
            seq_profiles.append(seq_profile)
            seq_profile = generates_ptask_profile(
                name=seq_name + "_cpu", com=empty_comm, cpu=splitted_comp_matrix
            )
            seq_profiles.append(seq_profile)

    # The needed mapping for the batsim job
    (mapping_ranktohost, hosts) = profile_mapping_from_smpi_metadata(metadata)

    (root_profile, profile_sequence) = generates_ptask_profile_sequence(
        profile_name, profiles=seq_profiles, repeat=1
    )

    return (root_profile, profile_sequence, mapping_ranktohost)
