#!/usr/bin/env Rscript

library(tidyverse);

args = commandArgs(trailingOnly=TRUE);

events = args[1];
result_file = args[2];

df_marks = read_csv(events)

bcast_enter = df_marks %>%
    filter(action_event == "begin" & action == "bcast") %>%
    select(-action, -action_event) %>%
    rename(bcast_begins = time)

bcast_exit  = df_marks %>%
    filter(action_event == "end" & action == "bcast") %>%
    select(-action, -action_event) %>%
    rename(bcast_ends = time)

comp_enter = df_marks %>% filter(action_event == "begin" & action == "comp") %>%
    select(-action, -action_event) %>%
    rename(comp_begins = time)

comp_exit  = df_marks %>% filter(action_event == "end" & action == "comp") %>%
    select(-action, -action_event) %>%
    rename(comp_ends = time)


events_inline = bcast_enter %>%
    full_join(bcast_exit, by=c("rank", "loop")) %>%
    full_join(comp_enter, by=c("rank", "loop")) %>%
    full_join(comp_exit,  by=c("rank", "loop")) %>%
    arrange(rank, loop)

loop_info = events_inline %>%
    group_by(loop) %>%
    summarize(
        max_bcast_begin = max(bcast_begins),
        min_bcast_begin = min(bcast_begins),
        max_bcast_end = max(bcast_ends),
        min_bcast_end = min(bcast_ends),

        max_comp_begins = max(comp_begins),
        min_comp_begins = min(comp_begins),
        max_comp_end = max(comp_ends),
        min_comp_end = min(comp_ends),
    ) %>% write_csv(result_file)

print(loop_info)