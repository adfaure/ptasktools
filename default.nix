{ pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/refs/tags/21.05.tar.gz") { }
, kapack ? import (fetchTarball
  "https://github.com/oar-team/nur-kapack/archive/refs/heads/master.tar.gz") { }
, old-kapack ? import (fetchTarball {
  name = "kapack";
  url =
    "https://github.com/oar-team/kapack/archive/f70b1149a9ae30fafe87af46e517985bacc331c3.tar.gz";
  sha256 = "sha256:007xyvrcfl9qfi7947kv3clbb7wjniicn1ks6wf2dlcpd2qzmh22";
}) { } }:

with pkgs;

python3Packages.buildPythonPackage rec {
  pname = "exptools";
  version = "1.0.0";
  name = "${pname}-${version}";

  buildInputs = [ python3Packages.pyyaml python3Packages.pandas ];

  propagatedBuildInputs = [
    (pkgs.rWrapper.override {
      packages = with pkgs.rPackages;
        [
          # add r packages here
          tidyverse
        ];
    })
  ];
  src = ./.;
  # src = pkgs.lib.sourceByRegex ./. [ "src" "setup.py" ];
}
