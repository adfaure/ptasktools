#!/usr/bin/env python
from distutils.core import setup

setup(
        name='exptools',
        version='0.1.0',
        python_requires='>=3.6',
        install_requires=["PyYAML", "pandas"],
        package_dir={'exptools': 'src'},
        packages = ["exptools"],
        include_package_data=True,
        package_data = {
            'exptools': ['scripts/*'],
        },
        description='Tools for batsim',
        author='Adrien Faure',
        author_email='adrien.faure@protonmail.com',
        url='https://github.com/adfaure/ptasktools',
        license='MIT',
        classifiers=[
            "Programming Language :: Python :: 3",
            "License :: OSI Approved :: MIT License",
        ],
)
